import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_app_firebase_unittest/Repository/comment/comment_base_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'comment_event.dart';
part 'comment_state.dart';

class CommentBloc extends Bloc<CommentEvent, CommentState> {
  final commentFirebaseRepository = new CommentFirebaseRepository();
  CommentBloc() :
        super(CommentInitial());

  @override
  Stream<CommentState> mapEventToState(
    CommentEvent event,
  ) async* {
    if(event is AddComment){
      yield CommentLoading();
      try{
        yield await commentFirebaseRepository.addComment(event.text)
            .then(
                (docRef) => docRef.get().then(
                    (docSnapshot) => CommentData(comment_text: docSnapshot.data()['comment_text']) )
        );
      }catch(e){
        print(e.toString());
      }

//      yield CommentData();
    }
  }
}
