import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_app_firebase_unittest/Comment/bloc/comment_bloc.dart';
import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';

class CommentPage extends StatelessWidget {
  TextEditingController commentController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CommentBloc(),
      child: BlocBuilder<CommentBloc, CommentState>(builder: (context, state){
      return Column(
        children: [
          TextField(
            controller: commentController,
          ),
          Text('Comment moi: ${state is CommentData ? state.comment_text : ''}'),
          RaisedButton(child: Text('Add Comment'), onPressed: (){
            return context.bloc<CommentBloc>().add(AddComment(commentController.text));
          })
        ],
      );
    })
    );
  }
}
