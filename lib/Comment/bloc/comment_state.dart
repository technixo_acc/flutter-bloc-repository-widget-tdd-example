part of 'comment_bloc.dart';

@immutable
abstract class CommentState {}

class CommentInitial extends CommentState {}


class CommentData extends CommentState {
  final String comment_text;

  CommentData({this.comment_text});

  CommentData copyWith({
    String comment_text
  }) {
    return CommentData (
      comment_text: comment_text ?? this.comment_text
    );
  }
}


class CommentLoading extends CommentState {

  CommentLoading();

}
