import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_app_firebase_unittest/Repository/HelloRepository.dart';

part 'hello_event.dart';
part 'hello_state.dart';

class HelloBloc extends Bloc<HelloEvent, HelloState> {
  final helloRepository = new HelloRepository();
  HelloBloc() : super(HelloInitial());

  @override
  Stream<HelloState> mapEventToState(
    HelloEvent event,
  ) async* {

    if(event is AddComment){
      final docRef = await helloRepository.addText(event.text);
      yield HelloCommentLastRef(refId: docRef.id);
    }

    if(event is LoadAllComments){

    }

    if(event is GetCommentById){
      final helloModel = await helloRepository.getText(event.refId);
      yield LoadCommentByIdResult(text: helloModel.text);
    }

  }
}
