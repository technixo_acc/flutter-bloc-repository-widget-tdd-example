part of 'hello_bloc.dart';

@immutable
abstract class HelloState {}

class HelloInitial extends HelloState {}

class HelloCommentState extends HelloState {
  final String text;

  HelloCommentState({this.text});

  HelloCommentState copyWith({
    String text
  }) {
    return HelloCommentState (
        text: text ?? this.text
    );
  }
}

class HelloCommentLastRef extends HelloState {
  final String refId;

  HelloCommentLastRef({this.refId});

  HelloCommentLastRef copyWith({
    String refId
  }) {
    return HelloCommentLastRef (
      refId: refId ?? this.refId
    );
  }
}

class LoadCommentByIdResult extends HelloState {
  final String text;

  LoadCommentByIdResult({this.text});

  LoadCommentByIdResult copyWith({
    String text
  }) {
    return LoadCommentByIdResult (
      text: text ?? this.text
    );
  }
}
