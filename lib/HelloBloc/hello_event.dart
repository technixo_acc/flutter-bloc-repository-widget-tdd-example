part of 'hello_bloc.dart';

@immutable
abstract class HelloEvent {}

class AddComment extends HelloEvent {
  String text;
  AddComment(this.text);
}

class LoadAllComments extends HelloEvent {

  LoadAllComments();
}

class GetCommentById extends HelloEvent {
  final String refId;

  GetCommentById(this.refId);
}