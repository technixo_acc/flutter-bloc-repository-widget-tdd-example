import 'package:firebase_core/firebase_core.dart';
//final FirebaseOptions firebaseOptions = const FirebaseOptions(
//    apiKey: "AIzaSyAgqBf_1zQIl73RCkJbnqv_Q7vMWzm_qt8",
//    authDomain: "technixovn.firebaseapp.com",
//    databaseURL: "https://technixovn.firebaseio.com",
//    projectId: "technixovn",
//    storageBucket: "technixovn.appspot.com",
//    messagingSenderId: "362809800688",
//    appId: "1:362809800688:web:2fbcc5773d8ab08868cf35",
//    measurementId: "G-MV554DDM1J"
//);
Future<void> initializeDefault() async {
  FirebaseApp app = await Firebase.initializeApp();
  assert(app != null);
  print('Initialized default app $app');
}