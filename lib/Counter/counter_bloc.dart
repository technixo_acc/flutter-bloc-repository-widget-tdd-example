import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'package:flutter_app_firebase_unittest/Counter/counter_event.dart';
part 'package:flutter_app_firebase_unittest/Counter/counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  CounterBloc() : super(CounterInitial());

  @override
  Stream<CounterState> mapEventToState(
    CounterEvent event,
  ) async* {

    if(event is Increment){
      yield NumberState(num: state.num + 1);
    }else if(event is Decrement){
      yield NumberState(num: state.num - 1);
    }

  }
}
