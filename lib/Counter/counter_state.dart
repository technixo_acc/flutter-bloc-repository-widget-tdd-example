part of 'counter_bloc.dart';

@immutable
abstract class CounterState {
  final int num = 0;

}

class CounterInitial extends CounterState {}

class NumberState extends CounterState {
  final int num;

  NumberState({this.num = 0});

  NumberState copyWith({
    int num
  }) {
    return NumberState (
        num: num ?? this.num
    );
  }
}
