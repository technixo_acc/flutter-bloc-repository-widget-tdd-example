import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';

class MockFirestore {
  MockFirestoreInstance instance = null;
  static final MockFirestore _singleton = new MockFirestore._internal();

  factory MockFirestore() {
    return _singleton;
  }

  MockFirestore._internal();

  void initFirestoreMock(){
    instance = MockFirestoreInstance();
  }

  void setInstance(MockFirestoreInstance _instance){
    instance = _instance;
  }

  MockFirestoreInstance getInstance() => instance;
}