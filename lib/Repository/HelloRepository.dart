import 'package:flutter_app_firebase_unittest/Model/HelloModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
abstract class HelloBaseRepository {
  final FirebaseFirestore firestore;


  //pass the firestore mocking instance for unit test
  HelloBaseRepository({FirebaseFirestore firestore}) : firestore = firestore ?? FirebaseFirestore.instance;

  Future<DocumentReference> addText(String text);
  Future<HelloModel> getText(String docid);
  Future<List<HelloModel>> getAll();
}

class HelloRepository implements HelloBaseRepository {
  final FirebaseFirestore firestore;

  HelloRepository({FirebaseFirestore firestore}) : firestore = firestore ?? FirebaseFirestore.instance;

  @override
  Future<DocumentReference> addText(String text) {
    return firestore.collection('hello').add({
      "text": text
    });
  }

  @override
  Future<HelloModel> getText(String docid) {
    return firestore.doc('hello/$docid').get().then((value) => new HelloModel().fromJson(value.data()));
  }

  @override
  Future<List<HelloModel>> getAll(){
    return firestore.collection('hello').get().then((snapshots) =>
        Future.wait(snapshots.docs.map((snapshot) => Future.value(new HelloModel().fromJson(snapshot.data())) ))
    );
  }

  
}