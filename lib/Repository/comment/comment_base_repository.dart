import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_app_firebase_unittest/Model/CommentModel.dart';
import 'package:flutter_app_firebase_unittest/mockFirestore.dart';
part 'comment_firebase_repository.dart';

abstract class CommentBaseRepository {
  Future<DocumentReference> addComment(String text);

  Future<DocumentSnapshot> getCommentById(String id);

  Future<List<CommentModel>> getAllComment();
}
