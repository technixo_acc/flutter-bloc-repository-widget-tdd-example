part of 'comment_base_repository.dart';

class CommentFirebaseRepository implements CommentBaseRepository{
  final FirebaseFirestore firestore = MockFirestore().getInstance() ?? FirebaseFirestore.instance;

  CommentFirebaseRepository();

  @override
  Future<DocumentReference> addComment(String text) {
    return firestore.collection('comment').add({
      "comment_text": text
    }).catchError((err){
      print('error');
    });

    // TODO: implement addComment
//    throw UnimplementedError();

  }

  @override
  Future<DocumentSnapshot> getCommentById(String id) {
    return firestore.doc('comment/$id').get();
  }

  @override
  Future<List<CommentModel>> getAllComment(){
    return firestore.collection('comment').get().then(
            (querySnapshot) => querySnapshot.docs.map(
                (documentSnapshot) => (new CommentModel().fromJson(documentSnapshot.data()))
            ).toList()
    );
  }


}