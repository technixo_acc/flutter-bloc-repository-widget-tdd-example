import 'package:flutter_app_firebase_unittest/mockFirestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_app_firebase_unittest/Comment/bloc/comment_bloc.dart';
void main(){

  group('Comment Bloc', () {
    MockFirestore().initFirestoreMock();

    blocTest(
      'emit thang tao comment thi tra ve refId',
      build: () => CommentBloc(),
      act: (bloc) => bloc.add(AddComment('Technixo Hoho')),
      expect: [isA<CommentLoading>(), isA<CommentData>()],
      verify: (bloc){
        final state = bloc.state;
        if(state is CommentData){
          print('comment text: ${state.comment_text}');
          expect(state.comment_text, equals('Technixo Hoho'));
        }
      }
    );
  });
}