import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_app_firebase_unittest/Comment/bloc/comment_view.dart';
import 'package:flutter_app_firebase_unittest/main.dart';
import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:flutter_app_firebase_unittest/mockFirestore.dart';


void main(){
  group('test case', () {

    MockFirestore().initFirestoreMock();

    testWidgets('test add comment', (WidgetTester widgetTester) async{
      await widgetTester.pumpWidget(MyApp());
      expect(find.text('Add Comment'), findsOneWidget);

      await widgetTester.enterText(find.byType(TextField), 'Hello QuangAnh');

//      await widgetTester.pump();

      await widgetTester.tap(find.text('Add Comment'));
      await widgetTester.pump();

      expect(find.text('Comment moi: Hello QuangAnh'), findsOneWidget);

    });
  });
}