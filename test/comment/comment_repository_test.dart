import 'package:flutter_app_firebase_unittest/mockFirestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_app_firebase_unittest/Repository/comment/comment_base_repository.dart';
import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';

void main(){
  final firestoreInstance = MockFirestoreInstance();
  MockFirestore().setInstance(firestoreInstance);


  final commentRepository = new CommentFirebaseRepository();
  group('tat ca comment test', () {
    test('Add comment', () async {
      final commentRef = await commentRepository  .addComment("hello technixo");
      expect(commentRef.id, isNotNull);
      print('document id ${commentRef.id}');
      //expect(1, equals(2));

      final documentSnapshot = await commentRepository.getCommentById(commentRef.id);
//    expect(commentGetRef.id, isNotNull);
      expect(documentSnapshot.data()["comment_text"], equals('hello technixo'));

    });
    test('get all comment', () async {
      await commentRepository.addComment("hello technixo 1111");
      await commentRepository.addComment("hello technixo 111122222");
      final listComment = await commentRepository.getAllComment();
      expect(listComment.length, greaterThanOrEqualTo(3));
      listComment.forEach((element){
        print('--- ${element.comment_text}');
      });

    });
  });

}