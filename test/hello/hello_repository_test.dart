import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_app_firebase_unittest/Repository/HelloRepository.dart';
import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';

void main() async{
  final firestore = MockFirestoreInstance();
  HelloRepository helloRepository = new HelloRepository(firestore: firestore);
  test('Add a document and get document text', () async {
    final documentRef = await helloRepository.addText('hello');
    print('--- id ${documentRef.id}');
    expect(documentRef.id, isNotEmpty);
    final helloModel = await helloRepository.getText(documentRef.id);
    expect(helloModel.text, equals('hello'));
    print('--- hello model text: ${helloModel.text}');
    await helloRepository.addText('hello 2');
    await helloRepository.addText('hello 3');
    await helloRepository.addText('hello 4');
    final allHelloModel = await helloRepository.getAll();
    expect(allHelloModel.length, greaterThanOrEqualTo(4));
    allHelloModel.forEach((element) {
      print('--- element text: ${element.text}');
    });
  });




}