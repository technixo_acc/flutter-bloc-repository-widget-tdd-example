// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_app_firebase_unittest/Counter/counter_bloc.dart';
import 'package:flutter_app_firebase_unittest/main.dart';

class MockCounterBloc extends MockBloc<CounterState> implements CounterBloc {}

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.text('Increment'));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);

    await tester.tap(find.text('Increment'));
    await tester.pump();
    expect(find.text('2'), findsOneWidget);

    await tester.tap(find.text('Decrement'));
    await tester.pump();
    expect(find.text('1'), findsOneWidget);
  });

  group('CounterBloc', (){
    blocTest<CounterBloc, CounterState>(
      'emit Increment should yield the state +1',
      build: () => CounterBloc(),
      act: (bloc) async => bloc.add(Increment()),
      expect: [isA<CounterState>()],
      verify: (bloc){
        print('--- current number is ' + bloc.state.num.toString());
        expect(bloc.state.num, 1);
      }
    );

    blocTest<CounterBloc, CounterState>(
        'Bloc should decrement the number correctly',
        build: () => CounterBloc(),
        act: (bloc) async => bloc.add(Decrement()),
        expect: [isA<CounterState>()],
        verify: (bloc){
          print('--- current number is ' + bloc.state.num.toString());
          expect(bloc.state.num, -1);
        }
    );
  });

}
